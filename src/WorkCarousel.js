import React, {Component} from 'react';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import openplatform from 'openplatform';
import Slick from 'react-slick';
import range from 'lodash.range';

const arrowStyle = {
  display: 'inline-block',
  background: 'black',
  color: 'white',
  zIndex: 100,
  opacity: '0.8',
  position: 'absolute',
  top: '30%',
  height: 44,
  width: 44,
  fontSize: 34,
  textAlign: 'center',
  borderRadius: 22,
  verticalAlign: 'middle',

  left: 'auto'
};
const PrevArrow = ({onClick}) => (
  <div
    style={{
      ...arrowStyle,
      left: 22,
      transform: 'scale(-1, 1)'
    }}
    onClick={onClick}>
    ➜
  </div>
);

const NextArrow = ({onClick}) => (
  <div style={{...arrowStyle, right: 22}} onClick={onClick}>
    ➜
  </div>
);

class WorkCarousel extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.loadWorks(props.config);
  }
  async loadWorks(config) {
    if (this.state.config === config) {
      return;
    }

    const resultCount = 12;
    this.state.config = config;
    this.state.works = range(resultCount).map(() => ({}));
    let j = 0;

    try {
      console.time('search ' + config.cql);
      await Promise.all(
        range(resultCount).map(async i => {
          const [work] = await openplatform.search({
            q: config.cql,
            fields: ['title', 'creator', 'pid', 'coverUrlFull'],
            offset: i,
            limit: 1
          });
          if (work && config === this.state.config) {
            const works = this.state.works.slice();
            works[j++] = work;
            this.setState({works});
          }
        })
      );
      console.timeEnd('search ' + config.cql);
    } catch (e) {
      console.error(e);
    }
  }
  render() {
    const props = this.props;
    const state = this.state;

    if (props.config !== state.config) {
      this.loadWorks(props.config);
    }

    const slickSettings = {
      slidesToShow: 5,
      slidesToScroll: 4,
      dots: true,
      infinite: true,
      nextArrow: <NextArrow />,
      prevArrow: <PrevArrow />
    };
    return (
      <div
        style={{
          width: '80%',
          margin: 40,
          display: 'inline-block'
        }}>
        <Slick {...slickSettings}>
          {state.works.map(({title, creator, pid, coverUrlFull}) => (
            <div key={pid || Math.random()}>
              {coverUrlFull ? (
                <img
                  style={{
                    width: '100%'
                  }}
                  src={coverUrlFull}
                  alt={`${title || ''} - ${creator || ''}`}
                />
              ) : (
                <center
                  style={{
                    border: '1px solid black',
                    margin: 3,
                    padding: 3
                  }}>
                  <strong>{title}</strong>
                  <hr />
                  <em>{creator}</em>
                </center>
              )}
            </div>
          ))}
        </Slick>
      </div>
    );
  }
}
export default WorkCarousel;
