import React, {Component} from 'react';
import WorkCarousel from './WorkCarousel';
import openplatform from 'openplatform';
import './App.css';

(async () => {
  console.time('openplatform-connect');
  await openplatform.connect(
    '9296679b-5c7a-4e2d-a554-4a65a3117feb',
    'f2a826284e5d3f2ff650e38a8fc6e6a4c1a0b075e57ffd43c85c159b874da2a8'
  );
  console.timeEnd('openplatform-connect');
})();

function CarouselConfig({config, onChange}) {
  return (
    <div>
      CQL:
      <input
        type="text"
        value={config.cql}
        onChange={e => {
          onChange(Object.assign({}, config, {cql: e.target.value}));
        }}
      />
    </div>
  );
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      config: {
        cql: 'ottesen and (roselil or hullerikkerne)'
      }
    };
  }

  render() {
    return (
      <div className="App">
        <CarouselConfig
          config={this.state.config}
          onChange={config => this.setState({config})}
        />
        <p>{JSON.stringify(this.state.config)}</p>
        <WorkCarousel config={this.state.config} />
      </div>
    );
  }
}

export default App;
